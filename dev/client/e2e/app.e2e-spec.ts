import { WebrtcPocPage } from './app.po';

describe('webrtc-poc App', () => {
  let page: WebrtcPocPage;

  beforeEach(() => {
    page = new WebrtcPocPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
