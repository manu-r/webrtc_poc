import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CallRequestDialogComponent } from './call-request-dialog.component';

describe('CallRequestDialogComponent', () => {
  let component: CallRequestDialogComponent;
  let fixture: ComponentFixture<CallRequestDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CallRequestDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CallRequestDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
