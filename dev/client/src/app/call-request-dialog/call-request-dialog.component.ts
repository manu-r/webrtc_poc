import { Component, OnInit, Inject } from '@angular/core';
import { MdDialog, MdDialogRef, MD_DIALOG_DATA } from '@angular/material';


@Component({
  selector: 'app-call-request-dialog',
  templateUrl: './call-request-dialog.component.html',
  styleUrls: ['./call-request-dialog.component.css']
})
export class CallRequestDialogComponent implements OnInit {

  constructor(public dialogRef: MdDialogRef<CallRequestDialogComponent>, @Inject(MD_DIALOG_DATA) public data: any) {

  }

  ngOnInit() {
  }
  accept() {
    this.dialogRef.close('accept');
  }

  reject() {
    this.dialogRef.close('reject');
  }
}
