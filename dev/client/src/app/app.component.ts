import { Component } from '@angular/core';
import { NgModel } from '@angular/forms';
import { MdDialog, MdSnackBar } from '@angular/material';
import * as io from 'socket.io-client';
import { ChatRoomJoinDialogComponent } from "./chat-room-join-dialog/chat-room-join-dialog.component";
import { DomSanitizer } from "@angular/platform-browser";
import { SignallingChannel } from './handler/signalling-channel';
import { CallRequestDialogComponent } from "./call-request-dialog/call-request-dialog.component";
import { RemoteStreamHandler, CallRequestHandler, CallResponseHandler, MeetingRoomHandler, CallHandler } from "./handler/interfaces";
import { Me } from "./handler/me";
import { MeetingRoom } from "./handler/meetingroom";
import { Participant } from "app/handler/participant";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements CallRequestHandler, CallResponseHandler, RemoteStreamHandler, MeetingRoomHandler {
  private me: Me;
  private remoteStreams: any[];
  private participants: Participant[];

  constructor(public snackBar: MdSnackBar, public dialog: MdDialog, private sanitizer: DomSanitizer) {
    this.remoteStreams = [];
    this.openJoinRoomDialog();
  }

  onCallRejected(): void {
    this.openSnackBar("Your call was rejected");
  }
  onCallAccepted(): void {
    this.openSnackBar("Your call was accepted");
  }
  onCallTimeout(): void {
    this.openSnackBar("Your call was timed out");
  }

  leaveRoom(): void {
    this.me.leaveRoom(this);
    this.openJoinRoomDialog();
  }

  openSnackBar(message): void {
    console.log(message);
    let snackBarRef = this.snackBar.open(message, '', {
      duration: 5000,
    });
  }

  openJoinRoomDialog(): void {
    let dialogRef = this.dialog.open(ChatRoomJoinDialogComponent, {
      width: '350px',
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(data => {
      if (data) {
        this.me = new Me(data.userName);
        this.me.joinRoom(data.room, this, this);
      } else {
        this.dialog.open(ChatRoomJoinDialogComponent, {
          width: '350px',
          disableClose: true
        });
      }
    });
  }
  //Meeting room handlers
  onParticipantList(participants) {
    this.participants = participants;
  }
  onNewJoinee(participant: Participant, room: string) {
    let newJoinee = new Participant(participant.id, participant.name, room);
    this.participants.push(newJoinee);
  }
  onJoining(room: string) {
    this.openSnackBar("You have joined the chatroom: " + room);
  }
  onLeaving(id: string, room: string) {
    //Do nothing
  }

  //Call handlers
  onCallRequest(id: string, callHandler: CallHandler) {
    let participant = null;
    for (let i = 0; i < this.participants.length; i++) {
      if (this.participants[i].id == id) {
        participant = this.participants[i];
      }
    }
    if (!participant) {
      callHandler.rejectCall(id);
    }
    let dialogRef = this.dialog.open(CallRequestDialogComponent, {
      width: '350px',
      data: { name: participant.name }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == "accept") {
        navigator.mediaDevices.getUserMedia({
          audio: true,
          video: true
        })
          .then((stream) => {
            document.getElementById('own-video')["srcObject"] = stream;
            callHandler.acceptCall(id, stream, this);
          })
          .catch((error) => {
            console.log(error);
            this.openSnackBar("Some error occured while getting local video");
          });
      } else {
        callHandler.rejectCall(id);
      }
    });
    return;
  }

  call(id) {
    navigator.mediaDevices.getUserMedia({
      audio: true,
      video: true
    })
      .then((stream) => {
        document.getElementById('own-video')["srcObject"] = stream;
        SignallingChannel.getInstance().call(id, stream, this, this);
      })
      .catch((error) => {
        this.openSnackBar("Some error occured while getting local video");
      });
  }


  onRemoteStream(peer: string, stream) {
    let unsafeURL = window.URL.createObjectURL(stream);
    console.warn('unsafe url: ' + unsafeURL);
    this.remoteStreams.push({
      "id": stream.id,
      "src": this.sanitizer.bypassSecurityTrustResourceUrl(unsafeURL)
    });
  }

  getMyself(): Me {
    if (!this.me) {
      this.me = new Me('Anonymous ' + Math.random() * 100);
    }
    return this.me;
  }
  getString(): string {
    return this.me.getRoom().getString();
  }
  getParticipant(id: string): Participant {
    for (let i = 0; i < this.participants.length; i++) {
      if (this.participants[i].id == id) {
        return this.participants[i];
      }
    }
    return null;
  }

  getParticipants(): Participant[] {
    return this.participants;
  }
}
