import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MdButtonModule, MdMenuModule, MdIconModule, MdListModule, MdSnackBarModule, MdDialogModule, MdInputModule } from '@angular/material';

import { AppComponent } from './app.component';
import { ChatRoomJoinDialogComponent } from './chat-room-join-dialog/chat-room-join-dialog.component';
import { CallRequestDialogComponent } from './call-request-dialog/call-request-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    ChatRoomJoinDialogComponent,
    CallRequestDialogComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    MdButtonModule, MdMenuModule, MdIconModule, MdListModule, MdSnackBarModule, MdDialogModule, MdInputModule
  ],
  exports: [
    MdButtonModule, MdMenuModule, MdIconModule, MdListModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [ ChatRoomJoinDialogComponent, CallRequestDialogComponent ]
})
export class AppModule { }
