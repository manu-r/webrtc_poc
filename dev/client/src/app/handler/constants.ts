export const constants = {
  severity: {
    error: 1,
    warn: 2,
    info: 3,
    log: 4
  },
  events: {
    message: 'message',
    //actions
    join: 'join',
    leave: 'leave',
    requestCall: 'request_call',
    respondCall: 'call_response',
    answer: 'answer',
    offer: 'offer',
    //notifications
    newJoinee: 'new_joinee',
    joined: 'joined',
    left: 'left',
    callRequest: 'call_request',
    callResponse: 'call_response',
    //information
    participants: 'participant_list',
    processCall: 'process_call',
    heartBeat: 'tick',
    getParticipants: 'get_participants',
    candidate: 'candidate'
  },
  callResponse: {
    accepted: 'accepted',
    rejected: 'rejected'
  }
}
