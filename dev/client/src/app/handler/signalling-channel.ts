import * as io from 'socket.io-client';
import ConnectionHandler from "./connection-handler";
import { config } from "./config";
import { CallHandler, MessageHandler, MeetingRoomHandler, CallRequestHandler, CallResponseHandler, RemoteStreamHandler } from "./interfaces";
import { constants } from "./constants";

export class SignallingChannel implements CallHandler, MessageHandler {
  private static signallingChannel: SignallingChannel;

  private socket: SocketIOClient.Socket;
  private messageHandler: MessageHandler;
  private callRequestHandler: CallRequestHandler;
  private meetingRoomHandler: MeetingRoomHandler;

  private constructor() {
    this.socket = io.connect();
    this.socket.on(constants.events.message, (message, severity) => {
      switch (severity) {
        case constants.severity.error:
          this.error(message);
          break;
        case constants.severity.warn:
          this.warn(message);
          break;
        case constants.severity.info:
          this.info(message);
          break;
        default:
          this.log(message);
      }
    });
    this.socket.on(constants.events.joined, (room: string) => {
      if (this.meetingRoomHandler)
        this.meetingRoomHandler.onJoining(room);
    });
    this.socket.on(constants.events.participants, (participants) => {
      if (this.meetingRoomHandler)
        this.meetingRoomHandler.onParticipantList(participants);
    });
    this.socket.on(constants.events.newJoinee, (participant, room) => {
      if (this.meetingRoomHandler)
        this.meetingRoomHandler.onNewJoinee(participant, room);
    });
    this.socket.on(constants.events.left, (participant, room) => {
      if (this.meetingRoomHandler)
        this.meetingRoomHandler.onLeaving(participant, room);
    });

    this.socket.on(constants.events.callRequest, (id) => {
      if (this.callRequestHandler) {
        this.callRequestHandler.onCallRequest(id, this);
      }
    });
  }

  static getInstance(): SignallingChannel {
    if (!SignallingChannel.signallingChannel) {
      SignallingChannel.signallingChannel = new SignallingChannel();
    }
    return SignallingChannel.signallingChannel;
  }

  // Message Handler  =======================================
  error(message: string): void {
    console.error(message);
    if (this.messageHandler) {
      this.messageHandler.error(message);
    }
  }
  warn(message: string): void {
    console.warn(message);
    if (this.messageHandler) {
      this.messageHandler.warn(message);
    }
  }
  info(message: string): void {
    console.info(message);
    if (this.messageHandler) {
      this.messageHandler.info(message);
    }
  }
  log(message: string): void {
    console.log(message);
    if (this.messageHandler) {
      this.messageHandler.log(message);
    }
  }
  // Message Handler  =======================================

  // Meeting Room ===========================================
  joinRoom(room: string, user: string, meetingRoomHandler: MeetingRoomHandler, callRequestHandler: CallRequestHandler): void {
    this.meetingRoomHandler = meetingRoomHandler;
    this.callRequestHandler = callRequestHandler;
    this.socket.emit(constants.events.join, room, user);
  }
  leaveRoom(room: string, meetingRoomHandler: MeetingRoomHandler): void {
    this.meetingRoomHandler = meetingRoomHandler;
    this.socket.emit(constants.events.leave, room);
  }
  tick() {
    this.socket.emit(constants.events.heartBeat);
  }
  getParticipants(room) {
    this.socket.emit(constants.events.getParticipants, room);
  }
  // Meeting Room ===========================================
  call(id: string, localStream, responseHandler: CallResponseHandler, remoteStreamHandler: RemoteStreamHandler): void {
    let timer = setTimeout(function() {
      responseHandler.onCallTimeout();
    }, config.call.timeout);
    this.socket.emit(constants.events.requestCall, id);
    this.socket.on(constants.events.callResponse, (response) => {
      clearTimeout(timer);
      if (response == constants.callResponse.accepted) {
        responseHandler.onCallAccepted();
        this.offer(id, localStream, remoteStreamHandler);
      } else {
        responseHandler.onCallRejected();
      }
    });
  }
  rejectCall(id: string) {
    this.socket.emit(constants.events.respondCall, id, constants.callResponse.rejected);
  }
  acceptCall(id: string, localStream, remoteStreamHandler: RemoteStreamHandler) {
    this.socket.emit(constants.events.respondCall, id, constants.callResponse.accepted);
    this.socket.on(constants.events.offer, (data) => {
      data = JSON.parse(data);
      this.answer(id, data, localStream, remoteStreamHandler);
    });
  }

  offer(id: string, localStream, remoteStreamHandler: RemoteStreamHandler) {
    let connectionHandler = ConnectionHandler.getInstance(this, id, this);
    connectionHandler.makeOffer(localStream, remoteStreamHandler);
    this.socket.on(constants.events.candidate, (candidate) => {
      candidate = JSON.parse(candidate);
      connectionHandler.addIceCandidate(candidate);
    });
    this.socket.on(constants.events.answer, (remoteDescription) => {
      remoteDescription = JSON.parse(remoteDescription);
      connectionHandler.setRemoteDescription(remoteDescription);
    })
  }
  answer(id: string, data: any, localStream, remoteStreamHandler: RemoteStreamHandler) {
    let connectionHandler = ConnectionHandler.getInstance(this, id, this);
    connectionHandler.sendAnswer(localStream, data, remoteStreamHandler);
    this.socket.on(constants.events.candidate, (candidate) => {
      candidate = JSON.parse(candidate);
      connectionHandler.addIceCandidate(candidate);
    });
  }
  //======== Called by the ConnectionHandler ===============
  sendOffer(id: string, data: any) {
    this.socket.emit(constants.events.offer, id, data);
  }
  sendAnswer(id: string, data: any) {
    this.socket.emit(constants.events.answer, id, JSON.stringify(data));
  }
  sendCandidate(id: string, data: any) {
    this.socket.emit(constants.events.candidate, id, data);
  }
  // Meeting Room ===========================================

  // Setter Methods =========================================
  setMessageHandler(messageHandler: MessageHandler) {
    this.messageHandler = messageHandler;
  }
  // Setter Methods =========================================
}
