import { Participant } from "./participant";
import { MeetingRoomHandler } from "./interfaces";
import { SignallingChannel } from "./signalling-channel";

export class MeetingRoom implements MeetingRoomHandler {
  private static meetingRoom: MeetingRoom;
  private participants: Participant[];

  private constructor(public room: string, public meetingRoomHandler?: MeetingRoomHandler) { }

  static getInstance(room: string, meetingRoomHandler?: MeetingRoomHandler): MeetingRoom {
    if(!MeetingRoom.meetingRoom) {
      MeetingRoom.meetingRoom = new MeetingRoom(room, meetingRoomHandler);
    }
    return MeetingRoom.meetingRoom;
  }

  onJoining(room: string): void {
    if (this.meetingRoomHandler) {
      this.meetingRoomHandler.onJoining(room);
    }
  }
  onNewJoinee(participant: Participant, room: string): void {
    this.participants.push(new Participant(participant.id, participant.name, room));
    if (this.meetingRoomHandler) {
      this.meetingRoomHandler.onNewJoinee(participant, room);
    }
  }
  onLeaving(participant: string, room: string): void {
    for (let i = 0; i < this.participants.length; i++) {
      if (this.participants[i].id == participant) {
        this.participants.splice(i, 1);
      }
    }
    if (this.meetingRoomHandler) {
      this.meetingRoomHandler.onLeaving(participant, room);
    }
  }
  onParticipantList(participants: Participant[]): void {
    this.participants = participants;
    if (this.meetingRoomHandler) {
      this.meetingRoomHandler.onParticipantList(participants);
    }
  }
  getString(): string {
    return this.room;
  }
  getParticipant(id: string): Participant {
    for (let i = 0; i < this.participants.length; i++) {
      if (this.participants[i].id == id) {
        return this.participants[i];
      }
    }
    return null;
  }

  getParticipants(): Participant[] {
    return this.participants;
  }
}
