export const config = {
  server: {
    iceServers: [{
      'urls': 'stun:stun.l.google.com:19302'
    },
    {
      'urls': 'stun:stun1.l.google.com:19302'
    }]
  },
  call: {
    timeout: 30000
  },
  hearBeatInterval: 10000
}
