import { SignallingChannel } from "./signalling-channel";
import { CallHandler, RemoteStreamHandler, CallResponseHandler } from "./interfaces";

export class Participant {
  constructor(public id: string, public name: string, public room: string){};

  call(localStream: any, responseHandler: CallResponseHandler, remoteStreamHandler: RemoteStreamHandler): void {
    SignallingChannel.getInstance().call(this.id, localStream, responseHandler, remoteStreamHandler);
  }
}
