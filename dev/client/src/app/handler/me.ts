import { config } from "./config";
import { MeetingRoom } from "./meetingroom";
import { MeetingRoomHandler, CallRequestHandler } from "./interfaces";
import { SignallingChannel } from "./signalling-channel";

export class Me {
  room: MeetingRoom;

  constructor(public name: string) {
    this.startHeartBeat();
  }

  joinRoom(room: string, meetingRoomHandler: MeetingRoomHandler, callRequestHandler: CallRequestHandler) {
    this.room = MeetingRoom.getInstance(room);
    SignallingChannel.getInstance().joinRoom(room, this.name, meetingRoomHandler, callRequestHandler);
  }

  leaveRoom(meetingRoomHandler: MeetingRoomHandler) {
    SignallingChannel.getInstance().leaveRoom(this.room.getString(), meetingRoomHandler);
  }

  private startHeartBeat() {
    setInterval(function() {
      SignallingChannel.getInstance().tick();
    }, config.hearBeatInterval)
  }

  getName(): string {
    return this.name;
  }

  getRoom(): MeetingRoom {
    if(!this.room) {
      return MeetingRoom.getInstance('');
    }
    return this.room
  }
}
