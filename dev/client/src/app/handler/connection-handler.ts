import { config } from "./config";
import { SignallingChannel } from './signalling-channel';
import { RemoteStreamHandler, MessageHandler } from "./interfaces";

export default class ConnectionHandler {
  private static connectionHandler: ConnectionHandler;
  private peerConnection: RTCPeerConnection;

  private constructor(public signallingChannel: SignallingChannel, public peer: string, public messageHandler: MessageHandler) {
    this.signallingChannel = signallingChannel;
    this.peer = peer;
  }

  static getInstance(signallingChannel: SignallingChannel, peer: string, messageHandler: MessageHandler): ConnectionHandler {
    if (!ConnectionHandler.connectionHandler) {
      ConnectionHandler.connectionHandler = new ConnectionHandler(signallingChannel, peer, messageHandler);
    }
    return ConnectionHandler.connectionHandler;
  }

  makeOffer(localStream: any, remoteStreamHandler: RemoteStreamHandler) {
    console.log('making offer');
    this.peerConnection = new RTCPeerConnection(config.server);

    this.peerConnection.onicecandidate = (event) => {
      console.log('sending candidate fron caller')
      if (event.candidate)
        this.signallingChannel.sendCandidate(this.peer, JSON.stringify(event.candidate));
    };

    this.peerConnection.onaddstream = (event) => {
      console.log('new stream added on caller');
      remoteStreamHandler.onRemoteStream(this.peer, event.stream);
    }

    console.log('adding local stream');
    this.peerConnection.addStream(localStream);

    this.peerConnection.createOffer()
      .then((offer) => {
        console.log('creating offer')
        this.peerConnection.setLocalDescription(offer)
          .then(() => {
            console.log('sending offer')
            console.log(offer)
            this.signallingChannel.sendOffer(this.peer, JSON.stringify(offer));
          })
          .catch((error) => {
            console.error(error)
          });
      })
      .catch(function(error) {
        console.error(error);
      });
  }

  sendAnswer(localStream, remoteDescription, remoteStreamHandler: RemoteStreamHandler) {
    console.log('making answer')
    this.peerConnection = new RTCPeerConnection(config.server);
    this.peerConnection.setRemoteDescription(new RTCSessionDescription(remoteDescription))
      .then(() => {
        this.messageHandler.info("Set remote description at the receiver.")
        this.peerConnection.createAnswer()
          .then((localDescription) => {
            console.log('creating answer')
            this.peerConnection.setLocalDescription(localDescription, () => {
              console.log('sending answer')
              this.signallingChannel.sendAnswer(this.peer, localDescription);
            });
          })
          .catch((error) => {
            console.error(error);
          });
      })
      .catch((error) => {
        this.messageHandler.error(error);
      });

    this.peerConnection.onicecandidate = (event) => {
      if (event.candidate)
        console.log('adding ice candidate in receiver')
      this.signallingChannel.sendCandidate(this.peer, JSON.stringify(event.candidate));
    };

    this.peerConnection.onaddstream = (event) => {
      console.log('new stream added at receiver')
      remoteStreamHandler.onRemoteStream(this.peer, event.stream);
    }
    console.log('adding local stream');
    this.peerConnection.addStream(localStream);
  }

  addIceCandidate(candidate) {
    console.log('new candidate received from remote connection \r\n' + JSON.stringify(candidate));
    if (!candidate) {
      return;
    }
    this.peerConnection.addIceCandidate(new RTCIceCandidate(candidate))
      .then(() => {
        console.log('added new ice candidate');
      })
      .catch((error) => {
        console.error("adding candidate failed; candidate: \r\n" + JSON.stringify(candidate));
        console.error(error);
      })
  }

  setRemoteDescription(remoteDescription) {
    console.log('setting remote description');
    this.peerConnection.setRemoteDescription(new RTCSessionDescription(remoteDescription))
      .then(() => {
        this.messageHandler.info("Set remote description at the caller.")
      })
      .catch((error) => {
        this.messageHandler.error(error);
      });
  }
}
