import { Participant } from "./participant";

export interface CallResponseHandler {
  onCallRejected(): void;
  onCallAccepted(): void;
  onCallTimeout(): void;
}

export interface CallRequestHandler {
  onCallRequest(id: string, callHandler: CallHandler): void;
}

export interface CallHandler {
  call(id: string, localStream, responseHandler: CallResponseHandler, remoteStreamHandler: RemoteStreamHandler);
  acceptCall(id: string, localStream: any, remoteStreamHandler: RemoteStreamHandler);
  rejectCall(id: string);
}

export interface RemoteStreamHandler {
  onRemoteStream(id: string, stream: any): void;
}

export interface MessageHandler {
  error(message: string): void;
  warn(message: string): void;
  info(message: string): void;
  log(message: string): void;
}

export interface MeetingRoomHandler {
  onJoining(room: string): void;
  onNewJoinee(participant: Participant, room: string): void;
  onLeaving(participant: string, room: string): void;
  onParticipantList(participants: Participant[]): void;
}
