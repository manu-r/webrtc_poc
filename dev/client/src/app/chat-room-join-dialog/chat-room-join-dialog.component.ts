import { Component, OnInit, Inject } from '@angular/core';
import { MdDialogRef, MD_DIALOG_DATA } from "@angular/material/dialog";

@Component({
  selector: 'app-chat-room-join-dialog',
  templateUrl: './chat-room-join-dialog.component.html',
  styleUrls: ['./chat-room-join-dialog.component.css']
})
export class ChatRoomJoinDialogComponent implements OnInit {
  chat;

  constructor(public dialogRef: MdDialogRef<ChatRoomJoinDialogComponent>, @Inject(MD_DIALOG_DATA) public data: any) {
    this.chat = { };
  }

  ngOnInit() {
  }

  isEmpty(str) {
    if( !str || str == null || str.trim().length === 0 ) {
      return true;
    }
    return false;

  }
}
