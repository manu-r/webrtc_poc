import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatRoomJoinDialogComponent } from './chat-room-join-dialog.component';

describe('ChatRoomJoinDialogComponent', () => {
  let component: ChatRoomJoinDialogComponent;
  let fixture: ComponentFixture<ChatRoomJoinDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChatRoomJoinDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatRoomJoinDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
