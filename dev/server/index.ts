import * as express from 'express';
import * as http from 'http';
import * as path from 'path';
import * as socket from 'socket.io';

let app = express();
let participants = {};

app.use('/', express.static(path.join(__dirname, '/../client')));

app.get('/**', function(req, res, next) {
  res.sendFile('./index.html', { root: path.join(__dirname, '/../client') }, function(err) {
    if (err) {
      console.log(err);
    }
  });
});

let server = http.createServer(app).listen(8080, function() {
  console.log("Server started on port 8080");
})

let io = socket.listen(server);

io.sockets.on('connection', function(socket) {
  //Meeting room handlers
  socket.on('join', (room, userName) => {
    if (socket.room) {
      socket.emit('message', 'You are already in the room', 'info');
    }
    socket.join(room);
    if (!participants[room]) {
      participants[room] = [];
    }
    socket.send('You have joined the chatroom');
    socket.emit('joined', room);
    socket.emit('participant_list', participants[room]);

    participants[room].push({
      "id": socket.id,
      "name": userName
    })

    socket.to(room).emit('message', userName + ' joined the chatroom', 'info');
    socket.to(room).emit('new_joinee', { "id": socket.id, "name": userName }, room);
  });

  socket.on('message', (id, message) => {
    socket.to(id).emit('message', message);
  })

  socket.on('leave', (room, userName) => {
    socket.leave(room);
    socket.send('You have left the room');

    socket.to(room).emit('left', socket.id);
    for (let i = 0; i < participants[room].length; i++) {
      if (participants[room][i].id == socket.id) {
        participants[room].splice(i, 1);
      }
    }
    socket.to(room).emit('message', userName + ' has left the chatroom', 'info');
  });

  //Call handlers
  socket.on('call', (id, data) => {
    socket.to(id).emit('call', socket.id, data);
  });

  socket.on('request_call', (id) => {
    socket.to(id).emit('call_request', socket.id);
  });

  socket.on('call_response', (id, response) => {
    socket.to(id).emit('call_response', response);
  });

  socket.on('candidate', (id, candidate) => {
    socket.to(id).emit('candidate', candidate);
  });

  socket.on('offer', (id, offer) => {
    socket.to(id).emit('offer', offer);
  });

  socket.on('answer', (id, answer) => {
    socket.to(id).emit('answer', answer);
  });
});
